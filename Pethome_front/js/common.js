//添加原型
Vue.prototype.$http = axios;

// 设置公共的请求路径
axios.defaults.baseURL="api.pethome.com"

//dfsUrl的公共路径
let baseDfsUrl = "http://121.37.194.36";



/*给axios请求添加一个前置拦截器*/
axios.interceptors.request.use(config=>{
    //携带token
    let uToken =  localStorage.getItem("token");
    if(uToken){
        //我就在请求头里面添加一个头信息叫做U-TOKEN
        config.headers['U-TOKEN']=uToken;
    }
    return config;
},error => {
    Promise.reject(error);
})



//2 使用axios后置拦截器，处理没有登录请求
axios.interceptors.response.use(res=>{
    //console.log("===============================",res)
    let {success, result} = res.data;
    console.debug(result)
    //用户没有登录
    if(!success && result==="noUser"){
        location.href = "/login.html";
    }
    //后台redis用户已经过期了
    if(!success && result==="expireUser"){
        localStorage.removeItem("token");
        localStorage.removeItem("loginUser");
        location.href = "/login.html";
    }
    return res;
},error => {
    Promise.reject(error);
})


/**
* 动态获取url地址?后面的参数，它会把参数封装成一个json对象
* @returns {Object}
*/
function getParam() {
    var url = location.search; //获取url中"?"符后的字串
    var theRequest = new Object();
    if (url.indexOf("?") != -1) {
        var str = url.substr(1);
        strs = str.split("&");
        for(var i = 0; i < strs.length; i ++) {
            theRequest[strs[i].split("=")[0]]=unescape(strs[i].split("=")[1]); }
    }
    return theRequest;
}