import babelpolyfill from 'babel-polyfill'
/*模块化引入vue*/
import Vue from 'vue'
import App from './App'
/*模块化引入elementui*/
import ElementUI from 'element-ui'
/*模块化引入elementui的css*/
import 'element-ui/lib/theme-default/index.css'
//import './assets/theme/theme-green/index.css'
/*模块化引入路由插件*/
import VueRouter from 'vue-router'
import store from './vuex/store'
import axios from 'axios'
import Vuex from 'vuex'
//import NProgress from 'nprogress'
//import 'nprogress/nprogress.css'
import routes from './routes'
// import Mock from './mock'
// Mock.bootstrap();
// Mock.bootstrap();
import 'font-awesome/css/font-awesome.min.css'
import VueQuillEditor from 'vue-quill-editor'


//配置axios的全局基本路径
axios.defaults.baseURL='http://localhost'
//全局属性配置，在任意组件内可以使用this.$http获取axios对象
Vue.prototype.$http = axios



Vue.use(ElementUI)
Vue.use(VueRouter)
Vue.use(Vuex)

//NProgress.configure({ showSpinner: false });

const router = new VueRouter({
    routes
})

// router.beforeEach((to, from, next) => {
//     //NProgress.start();
//     if (to.path == '/login') {
//         sessionStorage.removeItem('user');
//     }
//     let user = JSON.parse(sessionStorage.getItem('user'));
//     if (!user && to.path != '/login') {
//         next({path: '/login'})
//     } else {
//         next()
//     }
// })

//router.afterEach(transition => {
//NProgress.done();
//});

new Vue({
    //el: '#app',
    //template: '<App/>',
    router,
    store,
    //components: { App }
    render: h => h(App)
}).$mount('#app')

